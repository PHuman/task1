package test.by.training.humenuik.action;


import by.training.humenuik.action.DevicesAction;
import by.training.humenuik.entity.*;
import by.training.humenuik.exception.DeviceNotFoundException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;


public class DevicesActionTest {
    final static int ATLANT_POWER = 300;

    private ArrayList<ElectricDevice> createTestList() {
        ArrayList<ElectricDevice> devicesList = new ArrayList<>();
        devicesList.add(new Icebox("Atlant", ATLANT_POWER, false, 2));
        devicesList.add(new DVD("BBK", 100, true, "R"));
        devicesList.add(new Toaster("Polaris", 800, true, 2));
        devicesList.add(new CoffeeMaker("Bosch", 780, false, 1.5));
        return devicesList;
    }

    @Test
    public void sumOfPowerTest() {
        ArrayList<ElectricDevice> list = createTestList();
        int actual = DevicesAction.sumOfPower(list);
        int expected = 0;
        for (ElectricDevice device : list) {
            if (device.isSwitched()) {
                expected += device.getPower();
            }
        }
        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void sortByPowerTest() {
        ArrayList<ElectricDevice> list = createTestList();
        DevicesAction.sortByPower(list);
        for (int i = 0; i < list.size() - 2; i++) {
            Assert.assertTrue(list.get(i).getPower() <= list.get(i + 1).getPower());
        }
    }

    @Test
    public void searchByPowerTest() {
        ArrayList<ElectricDevice> list = createTestList();
        ElectricDevice device = null;
        try {
            device = DevicesAction.searchByPower(list, ATLANT_POWER);
        } catch (DeviceNotFoundException e) {
            e.printStackTrace();
        }
        Assert.assertSame(device, list.get(0));
    }

    @Test
    public void searchByPowerTestTwo() {
        ArrayList<ElectricDevice> list = createTestList();
        ElectricDevice actual = null;
        try {
            actual = DevicesAction.searchByPower(list, ATLANT_POWER);
        } catch (DeviceNotFoundException e) {
            e.printStackTrace();
        }
        ElectricDevice expected;
        for (int i = 0; i < list.size()-1; i++) {
            if (list.get(i).getPower() == ATLANT_POWER) {
                expected = list.get(i);
                Assert.assertSame(expected, actual);
            }
        }

    }
}
