package by.training.humenuik.serialization;

import by.training.humenuik.entity.TV;
import org.apache.log4j.Logger;

import java.io.*;

public class SerializationExample {
    static final Logger LOG = Logger.getLogger(SerializationExample.class);

    public boolean serialization(TV tv, String fileName) {
        boolean flag = false;
        File f = new File(fileName);
        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream(f);
            ostream = new ObjectOutputStream(fos);
            ostream.writeObject(tv);
            flag = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ostream != null) {
                    ostream.close();
                }
            } catch (IOException e) {
                LOG.error(e);
            }
        }
        return flag;
    }

    public TV deserialization(String fileName) throws InvalidObjectException {
        File fr = new File(fileName);
        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream(fr);
            istream = new ObjectInputStream(fis);
            return (TV) istream.readObject();
        } catch (ClassNotFoundException | FileNotFoundException | InvalidClassException e) {
            LOG.error(e);
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException e) {
                LOG.error(e);
            }
        }
        throw new InvalidObjectException("InvalidObjectException");
    }
}


