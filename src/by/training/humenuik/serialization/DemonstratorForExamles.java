package by.training.humenuik.serialization;

import by.training.humenuik.entity.TV;
import org.apache.log4j.Logger;

import java.io.InvalidObjectException;

public class DemonstratorForExamles {
    static final Logger LOG = Logger.getLogger(SerializationExample.class);

    public static void demonstrateSerialization() {
        TV tv = new TV("Horizont", 200, true, 27);
        LOG.info(tv);
        String file = "files\\1.data";
        SerializationExample sz = new SerializationExample();
        boolean b = sz.serialization(tv,file);
        TV res = null;
        try {
            res = sz.deserialization(file);
        } catch (InvalidObjectException e) {
           LOG.error(e);
        }
        LOG.info(res);
    }

}
