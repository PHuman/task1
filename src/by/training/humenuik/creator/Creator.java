package by.training.humenuik.creator;

import by.training.humenuik.entity.*;
import java.util.ArrayList;


public class Creator {

    public static ArrayList<ElectricDevice> buildList() {
        ArrayList<ElectricDevice> list = new ArrayList<>();
        list.add(new CoffeeMaker("Philips", 900,true, 1));
        list.add(new ElectricRange("Gefest", 2000,false,2));
        list.add(new DVD("LG", 85, true, "RW"));
        list.add(new Icebox("Minsk", 350,false,2));
        list.add(new Telephone("Sony", 50,true,150));
        list.add(new Toaster("Vitek", 900,false,2));
        list.add(new TV("Horizont", 275, true,19));
        return list;
    }
}
