package by.training.humenuik.entity;

import java.io.Serializable;

public class TV extends ElectricDevice implements Serializable {
    private double diagonalSize;
    private static String testStaticParam = "Static";
    private transient String testTransientParam = "Transient";
    public final String TEST_FINAL_PARAM = "Final";

    public TV(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public TV(String name, int power, boolean switched, double diagonalSize) {
        super(name, power, switched);
        this.diagonalSize = diagonalSize;
    }


    public double getDiagonalSize() {
        return diagonalSize;
    }

    @Override
    public String toString() {
        return "TV" +
                " diagonalSize= " + diagonalSize
              +  ", testTransientParam= " + testTransientParam +
                ", testStaticParam= " + testStaticParam +
                ", TEST_FINAL_PARAM= " + TEST_FINAL_PARAM;
    }
}
