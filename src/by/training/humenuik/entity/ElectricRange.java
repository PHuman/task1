package by.training.humenuik.entity;

public class ElectricRange extends ElectricDevice {
    private int burner;

    public ElectricRange(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public ElectricRange(String name, int power, boolean switched, int burner) {
        super(name, power, switched);
        this.burner = burner;
    }

    public int getBurner() {
        return burner;
    }
}
