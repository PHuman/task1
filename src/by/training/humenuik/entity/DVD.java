package by.training.humenuik.entity;

public class DVD extends ElectricDevice {
    private String drive;

    public DVD(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public DVD(String name, int power, boolean switched, String drive) {
        super(name, power, switched);
        this.drive = drive;
    }

    public String getDrive() {
        return drive;
    }
}
