package by.training.humenuik.entity;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Toaster extends ElectricDevice {
    private int cellCount;

    public Toaster() {
    }

    public Toaster(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public Toaster(String name, int power, boolean switched, int cellCount) {
        super(name, power, switched);
        this.cellCount = cellCount;
    }

    public int getCellCount() {
        return cellCount;
    }


    @Override
    public String toString() {
        return "Toaster " +
                " cellCount= " + cellCount;
    }
}
