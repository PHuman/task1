package by.training.humenuik.entity;

import java.util.ArrayList;

public class Room {
    private ArrayList<ElectricDevice> devices;

    public Room(ArrayList<ElectricDevice> devices) {
        this.devices = devices;
    }

    public ArrayList<ElectricDevice> getDevices() {
        return devices;
    }
}
