package by.training.humenuik.entity;


public abstract class ElectricDevice {
    private String name;
    private int power;
    private boolean switched;

    public ElectricDevice() {
    }

    public ElectricDevice(String name, int power, boolean switched) {
        this.name = name;
        this.power = power;
        this.switched = switched;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public void switchOn() {
        this.switched = true;
    }

    public void switchOff() {
        this.switched = false;
    }

    public boolean isSwitched() {
        return switched;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " - " + getName();
    }
}
