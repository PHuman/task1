package by.training.humenuik.entity;

public class Telephone extends ElectricDevice {
    private int jornalCapasity;

    public Telephone(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public Telephone(String name, int power, boolean switched, int jornalCapasity) {
        super(name, power, switched);
        this.jornalCapasity = jornalCapasity;
    }

    public int getJornalCapasity() {
        return jornalCapasity;
    }
}
