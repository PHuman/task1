package by.training.humenuik.entity;

public class CoffeeMaker extends ElectricDevice {
    private double volume;

    public CoffeeMaker(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public CoffeeMaker(String name, int power, boolean switched, double volume) {
        super(name, power, switched);
        this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }
}
