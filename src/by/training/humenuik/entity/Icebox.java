package by.training.humenuik.entity;

public class Icebox extends ElectricDevice {
    private int chamber;

    public Icebox(String name, int power, boolean switched) {
        super(name, power, switched);
    }

    public Icebox(String name, int power, boolean switched, int chamber) {
        super(name, power, switched);
        this.chamber = chamber;
    }

    public int getChamber() {
        return chamber;
    }
}
