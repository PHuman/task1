package by.training.humenuik.main;

import by.training.humenuik.action.DevicesAction;
import by.training.humenuik.creator.Creator;
import by.training.humenuik.entity.Room;
import by.training.humenuik.exception.DeviceNotFoundException;
import by.training.humenuik.serialization.DemonstratorForExamles;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



public class Electricity {
    static {
        new PropertyConfigurator().configure("log4j.properties");
    }

    static final Logger LOG = Logger.getLogger(Electricity.class);

    public static void main(String[] args) {
        Room room = new Room(Creator.buildList());
        DevicesAction.sortByPower(room.getDevices()).forEach(LOG::debug);
        LOG.debug(DevicesAction.sumOfPower(room.getDevices()));
        try {
            LOG.debug(DevicesAction.searchByPower(room.getDevices(), 85));
        } catch (DeviceNotFoundException e) {
            LOG.error("parameter is not valid");
        }
        DemonstratorForExamles.demonstrateSerialization();

    }

}
