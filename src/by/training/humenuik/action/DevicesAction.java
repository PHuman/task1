package by.training.humenuik.action;

import by.training.humenuik.entity.ElectricDevice;
import by.training.humenuik.exception.DeviceNotFoundException;
import org.apache.log4j.Logger;

import java.util.List;


public class DevicesAction {
    static final Logger LOG = Logger.getLogger(DevicesAction.class);

    static public List<ElectricDevice> sortByPower(List<ElectricDevice> devices) {
        devices.sort((a, b) -> a.getPower() - b.getPower());//Comparator
        return devices;
    }

    static public int sumOfPower(List<ElectricDevice> devices) {
        return devices.stream().filter(ElectricDevice::isSwitched).//Predicate
                mapToInt(ElectricDevice::getPower).//Function
                sum();
    }

    static public ElectricDevice searchByPower(List<ElectricDevice> devices, int power) throws DeviceNotFoundException {
        return devices.stream().filter((d) -> d.getPower() == power). //Predicate
                peek(LOG::info).//Consumer
                findFirst().//Optional
                orElseThrow(DeviceNotFoundException::new);//Supplier
    }
}
